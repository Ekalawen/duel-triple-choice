Il s'agit d'un jeu de duel de cartes en 1 contre 1.
À chaque tour, chaque joueur pourra choisir soit de jouer la carte qu'il y a devant lui, soit de jouer l'une des 2 cartes face visible de la pioche. Sachant que s'il ne joue pas une carte, il laissera alors cette carte à son adversaire.

Le but est de descendre les 10 points de vie adverse à 0, ou d'atteindre 15 points de puissance pour dominer son adversaire.
